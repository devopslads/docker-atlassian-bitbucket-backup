FROM java:openjdk-8
MAINTAINER Michal Wolonkiewicz https://blog.wolonkiewi.cz


ENV BITBUCKET_BACKUP_INSTALL_DIR /opt/atlassian


# Check for new versions there: https://marketplace.atlassian.com/plugins/com.atlassian.stash.backup.client/versions
ADD https://marketplace.atlassian.com/download/plugins/com.atlassian.stash.backup.client/version/300200000 $BITBUCKET_BACKUP_INSTALL_DIR/bitbucket-backup-distribution.zip

RUN cd ${BITBUCKET_BACKUP_INSTALL_DIR} \
    && unzip -qq bitbucket-backup-distribution.zip

ENV WORKDIR ${BITBUCKET_BACKUP_INSTALL_DIR}/bitbucket-backup-client-3.2.0


ADD entrypoint.sh /entrypoint.sh


WORKDIR ${WORKDIR}


CMD ["/entrypoint.sh"]


